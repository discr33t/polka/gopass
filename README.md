# gopass

Manage gopass

## Dependencies

* [polka.go](https://gitlab.com/discr33t/polka/go.git)
  _The go role is only a dependency if you are installing the latest version of
  gopass. This using `go get` instead of `brew` as the install method. The go
  configs must be included in the users `playbook.yml` since no default configs
  are passed to the dependent role_

## Role Variables

* `version`:
    * Type: String
    * Usages: Install the `latest` or `stable` version
_By setting `version` to `latest` you are installing with `go get` from the HEAD
of the master branch. By setting `version` to `stable` you are installing the
latest release using `brew`._

```
gopass:
  version: latest
```

## Example Playbook

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - polka.gopass

## License

MIT
